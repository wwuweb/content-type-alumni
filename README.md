# Alumni
This feature contains the Alumni content type.

## Requirements
* Features

## Installation Instructions
These instructions are intended to accompany a Drupal 8 installation from the [Composer File](https://bitbucket.org/wwuweb/wwu_drupal8_composer). Follow all of those installation instructions before proceeding here.

### 1) Install Features Module
You must have the **Features** module installed and enabled. If you have not yet done this, follow these steps:

1. From your site root, download the **Features** module
        `lando composer require drupal/features`
1. Enable the module by navigating to the extend page (*site/admin/modules*) in your browser, selecting the **Features** and **Features UI** modules, and clicking **Install**
1. Click Continue when prompted to also install **Configuration Update Base**

### 2) Download this module
Clone this repository into your `site/web/modules/contrib/` folder

### 3) Install Other Modules
The alumni content type is dependent on a number of modules that are included in the install profile, but are not necessarily enabled by default. To find out if any of these modules still need to be installed, in your browser go to *site/admin/modules* and find the **Alumni** module under the **Other** heading. Click the text to expand, and install any of the indicated modules listed by *Requires*

  >If you run into errors while trying to enable these modules, try installing them one at a time rather than all at once.
### 4) Enable this module
After the required modules are installed, select this module from the *site/admin/modules* screen and click the Install button.
